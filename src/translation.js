(function () {
    'use strict';
    var translation = angular.module('translation', ['LocalStorageModule']);
    translation.service('translationService', [
        '$http',
        '$rootScope',
        'localStorageService',
        function ($http, $rootScope, localStorageService) {
            $rootScope.locale = localStorageService.get('locale');
            if ($rootScope.locale === null) {
                $rootScope.locale = 'en';
            }
            $rootScope.t = {};
            $http.get('/assets/translations/' + $rootScope.locale + '.json')
                .success(function () {
                    ;
                });
        }
    ]);
}(angular));
