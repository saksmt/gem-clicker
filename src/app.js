(function (angular) {
    'use strict';
    var app = angular.module('gemClicker', [ 'ngTouch', 'LocalStorageModule' ]);
    app.config([
        'localStorageServiceProvider',
        function (localStorageServiceProvider, $rootScope) {
            localStorageServiceProvider
                .setPrefix('gemClicker');
        }
    ]);

}(angular));